package pid

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

// 写入pid文件
func WriteFile(){
	d1, err := json.Marshal(os.Getpid())
	if err !=nil{
		panic(fmt.Sprintf("pid marshal err: %s", err.Error()))
		return
	}
	err = ioutil.WriteFile("pid", d1, 0666) //写入文件(字节数组)
	if err !=nil{
		panic("cannot write pid, err:"+err.Error())
	}
}

// 删除pid文件
func RemoveFile(){
	os.Remove("pid")
}